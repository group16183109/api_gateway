package main

import (
	"context"
	"log"
	"net"
	pb "test/genproto"

	"google.golang.org/grpc"
)

type server struct {
	pb.UnimplementedAddFunction_ServiceServer
}

func NewServer(message string) *server {
	return &server{}

}

func (s *server) AddFunction(ctx context.Context, request *pb.Request) (*pb.Reply, error) {
	answer := request.X + request.Y
	return &pb.Reply{Answer: answer}, nil
}
func main() {
	lis, err := net.Listen("tcp", ":8088")
	if err != nil {
		log.Fatal("failed to listen", err)
	}
	s := grpc.NewServer()
	pb.RegisterAddFunction_ServiceServer(s, NewServer("in the server"))
	log.Println(s.GetServiceInfo())
	log.Println("server listening in :8088")
	if err := s.Serve(lis); err != nil {
		log.Fatal("failed to serve", err)
	}

}
